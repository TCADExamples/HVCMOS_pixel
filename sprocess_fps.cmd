##lingxin.meng@cern.ch


##math coord.ucs
#set mipX 60
#set mipY 0

AdvancedCalibration 2013.12

init Adaptive tdr=n@previous@_msh.tdr

pdbSet Silicon Phosphorus ActiveModel None
pdbSet Silicon Boron ActiveModel None

pdbSet ImplantData LeftBoundary Reflect
pdbSet ImplantData RightBoundary Reflect

pdbSet Grid Adaptive 1
pdbSet Grid AdaptiveField Refine.Abs.Error     1e37
pdbSet Grid AdaptiveField Refine.Rel.Error     1e10
pdbSet Grid AdaptiveField Refine.Target.Length 10.0
pdbSet Grid SnMesh UseLines 1
pdbSet Grid SnMesh DelaunayType boxmethod

refinebox clear
##refinebox !keep.lines
##refinebox interface.mat.pairs = { Silicon Oxide } adaptive refine.max.edge=0.01 refine.min.edge=0.001 def.max.asinhdiff= 0.3
refinebox min= {0 0} max= {10 @bulkW@} refine.fields= { NetActive Boron Phosphorus} refine.min.edge = {0.05 0.05} refine.max.edge = {5 5}  def.max.asinhdiff= 0.5
refinebox min= {-0.5 0} max= {0.5 @bulkW@} min.normal.size = 0.0001 max.lateral.size = 1 normal.growth.ratio = 2.5 interface.mat.pairs = { Silicon Oxide  }
##refinebox materials = {Oxide} adaptive adaptive refine.max.edge=0.01 refine.min.edge=0.001 def.max.asinhdiff= 0.5
#if @particle@ == "on"
	refinebox min= {@mipY@ @mipX@-1} max= {@bulkT@ @mipX@+1} refine.min.edge = {0.05 0.001} refine.max.edge = {0.5 0.1} def.max.asinhdiff= 0.25
#endif
grid remesh


contact name = "HV" region = r.hvl1
contact name = "HV" region = r.hvl2 add
contact name = "HV" region = r.hvr1 add
contact name = "HV" region = r.hvr2 add

contact name = "VDD" region = r.vddl1
contact name = "VDD" region = r.vddl2 add
contact name = "VDD" region = r.vddr1 add
contact name = "VDD" region = r.vddr2 add
contact name = "VDD" region = r.vddmid1 add
contact name = "VDD" region = r.vddmid2 add
contact name = "VDD" region = r.vddmid3 add

contact name = "VSS" region = r.vss1
contact name = "VSS" region = r.vss2 add

struct tdr=n@node@

exit
