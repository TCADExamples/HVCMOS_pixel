set yPos 140
set myPlot Plot_n@node|-2@_des

if {@particle@ == off } {
	load_file @pwd@/n@node|-2@_des.tdr
	create_plot -dataset n@node|-2@_des
	select_plots {$myPlot}

	## create streamline
	##set_field_prop -plot $myPlot -geom n@node|-2@_des Abs(ElectricField-V) -show_bands
	##set_legend_prop -plot $myPlot -orientation horizontal
	##set_legend_prop -plot $myPlot -show_background
	##create_streamline -plot $myPlot -field ElectricField-V -p1 {0 0} -p2 {2 119.9} -direction both -nofpoints 99
	##export_view @pwd@/Plots/n@node|-2@_des.png -plots $myPlot -format png

	## depletion depth
	set_field_prop -plot $myPlot -geom n@node|-2@_des SpaceCharge -show_bands
	create_cutline -plot $myPlot -type x -at $yPos
	select_plots {$myPlot Plot_1}
	export_curves Curve_1 -plot Plot_1 -filename @pwd@/output/SC_@name@.plx -format plx -overwrite
}
exit
