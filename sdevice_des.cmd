##lingxin.meng@cern.ch


#set VDD 3.3
#set VSS 0
#set mipTime 0.5e-12


File {
	Grid = "@tdr@"
	Plot = "n@node@"
	Current = "rad@fluence@neq_@HV@V_@resistivity@ohm.plt"
}


Electrode{
	{ Name="HV" Voltage=0.0 }
	{ Name="VSS" Voltage=0.0 }
	{ Name="VDD" Voltage=0.0 }
}

Physics{
##	DriftDiffusion
	eQCvanDort
	AreaFactor=1
	Temperature = 300 
	Mobility(
		DopingDep
		HighFieldSaturation( GradQuasiFermi )
		Enormal
	)
	EffectiveIntrinsicDensity( SlotBoom )
	Recombination(
		SRH( DopingDep )
		##Auger
		##eAvalanche( CarrierTempDrive )
		##hAvalanche( Okuto )
	)

#if @particle@ != "off" 
	HeavyIon (
		Direction=(0, 1)
		Location=(@mipX@, @mipY@)
		Time=@mipTime@
		Length=@bulkT@
		Wt_hi=0.05
		LET_f=1.281741e-05
		Gaussian
		PicoCoulomb
	)
#endif

}


Physics ( MaterialInterface = "Silicon/Oxide" ) {
#if @fluence@ != 0
	##	charge after irradiation
	##	Charge( Conc = 0 )
		Traps (
			( FixedCharge Conc=1e12  )
		) 
	
#endif
	##	Negative Bias Temperature Instability degredation model
	##	NBTI ( 
	##		Conc = 1e12
	##		NumberOfSamples = 1000
	##		hSHEDistribution  
	##	)
	}

#if @fluence@ != 0
	## Radiation physics
	Physics ( material = "Silicon" ) {
		Traps (
			( Acceptor Level fromCondBand Conc=@<fluence*1.613>@ EnergyMid=0.42 eXsection=9.5E-15 hXsection=9.5E-14 )
			( Acceptor Level fromCondBand Conc=@<fluence*0.9>@ EnergyMid=0.46 eXsection=5E-15 hXsection=5E-14 )
			( Donor Level fromValBand Conc=@<fluence*0.9>@ EnergyMid=0.36 eXsection=3.23E-13 hXsection=3.23E-14 )
		) 
	}
#endif

Math {
        Number_Of_Threads=maximum
        Iterations=25
	RelErrControl
        Digits=6
        ##ExtendedPrecision
	Method=pardiso
	NotDamped=200
        Extrapolate
        CheckTransientError
        CurrentWeighting
##	RecBoxIntegr
	RecBoxIntegr(1e-2 10 1000)
	RhsFactor=1e30
	ParameterInheritance=None
## High field saturation and avalanche for low current region:
	CDensityMin= 10

	##ComputeIonizationIntegrals(WriteAll)
	##AvalPostProcessing
##	CNormPrint
}



Solve {
	Poisson
	Coupled{Poisson Electron Hole}
	QuasiStationary(
		InitialStep=1e-1
		MinStep=1e-6
		MaxStep=1
		Goal{ Name="HV" Voltage=@HV@}
		) {
		Coupled{ Poisson Electron Hole}
	}
			
	QuasiStationary(
		InitialStep=1e-1
		MinStep=1e-6
		MaxStep=1
		Goal{ Name="VDD" Voltage=@VDD@} ) {
		Coupled {Poisson Electron Hole}
	}
##	QuasiStationary(
##		InitialStep=1e-1
##		MinStep=1e-6
##		MaxStep=1
##		Goal {  MaterialInterface = "Silicon/Oxide" Model = Traps(0) Parameter = Conc Value = 1e12 }	 ) {
##		Coupled {Poisson Electron Hole}
##	}
		

##	CurrentPlot( Time = (range = (0 1) intervals = 200) )



#if @particle@ == "on"
	NewCurrentPrefix = "particle_"
	
	Transient(
		InitialTime = 0.0
		FinalTime = @mipTime@
		MaxStep = @mipTime@
		MinStep = 1e-15
		## generates @Intervals@ frames
	   Plot { Range = (0.0 @mipTime@) Intervals=10 } 
	){
		Coupled { Poisson Electron hole}
		# 200 points for plot
		CurrentPlot ( Time = (range = (0.0 @mipTime@) intervals = 200) )
	}
#endif

}


Plot{
	eDensity hDensity
	eCurrent hCurrent
	ElectricField/Vector
	Potential
	AvalancheGeneration
	eAlphaAvalanche hAlphaAvalanche
	SpaceCharge
	eTrappedCharge hTrappedCharge
	eGapStatesRecombination hGapStatesRecombination
##	eVelocity hVelocity
##	eDriftVelocity hDriftVelocity
##	eMobility hMobility
	Doping DonorConcentration AcceptorConcentration 
##	CurrentPotential
	InterfaceNBTICharge

#if @particle@ != "off"
	HeavyIonCharge HeavyIonGeneration HeavyIonChargeDensity
#endif
}
