##lingxin.meng@cern.ch


#set bulkT 100.0
#set bulkW 270.0
#set oxideT -0.1

#set res20 6.725e14
#set res80 1.667e14
#set res200 6.650e13
#set res1000 1.32766e13

;doping concentration values at each resistivity
(define at_res_20 @res20@)
(define at_res_80 @res80@)
(define at_res_200 @res200@)
(define at_res_1000 @res1000@)

; === GEOMETRIC SETTINGS ===
(define dndepth 6)
(define dpdepth 1.5)
(define sndepth 0.5)
(define spdepth sndepth)
(define nplusdepth 0.2)
(define pplusdepth nplusdepth)
(define aludepth -0.5)

(define gapW 8)
(define hvW 4)
(define ppW 3)

(define subpixW1 50)
(define subpixW2 46)
(define subpixW3 45)
(define structW (+ hvW (* 2 gapW) subpixW1) )

(define dnmidW 90)
(define dpmidW 52)
(define spmidW dpmidW)
(define snmidW 30.2)

(define gapmidW 2.7)
(define npmidW 1.2)
(define ppmidW 1.8)


; --- DEEP TUBS ---

(define dplL gapW)
(define dplR (+ dplL hvW) )

(define dnlL (+ dplR gapW) )
(define dnlR (+ dnlL subpixW1) )

(define dnmidL (+ dplR structW gapW) )
(define dnmidR (+ dnmidL dnmidW) )

(define dpmidL (+ dnmidL gapmidW npmidW 0.1 0.55) )
(define dpmidR (+ dpmidL dpmidW) )

(define dprL (+ dnmidR gapW) )
(define dprR (+ dprL hvW) )

(define dnrL (+ dprR gapW) )
(define dnrR (+ dnrL subpixW1))


; --- SHALLOW TUBS ---

(define snlL (+ dnlL 2))
(define snlR (+ snlL subpixW2))
(define snrL (+ dnrL 2))
(define snrR (+ snrL subpixW2))

(define snmidL (+ dpmidR 0.55) )
(define snmidR (+ snmidL snmidW) )
(define spmidL dpmidL)
(define spmidR dpmidR)


; --- n+ ---
(define nplL (+ snlL 0.5))
(define nplR (+ nplL subpixW3))
(define nprL (+ snrL 0.5))
(define nprR (+ nprL subpixW3))

; --- p+ ---
(define pplL (+ dplL 0.5))
(define pplR (+ pplL ppW))
(define pprL (+ dprL 0.5))
(define pprR (+ pprL ppW))


; --- ref position ---
(define refhvl (+ dplL 2))
(define refhvr (+ dprL 2))
(define refvddmid1 (+ snlL 2))
(define refvddl2 (- snlR 2))
(define refvddr1 (+ snrL 2))
(define refvddr2 (- snrR 2))


(set! process-up-direction "+z")
(sdegeo:set-auto-region-naming OFF)

; geometry
(sdegeo:create-rectangle (position 0 0 0.0 )  (position @bulkW@ @bulkT@ 0.0 ) "Silicon" "r.substrate")

; references
(sdedr:define-refeval-window "ref.substrate" "Rectangle"  (position 0 0 0) (position @bulkW@ @bulkT@ 0.0 ))

; dp layer, extra process by inverting the dntub mask (source: Mathieu with AMS)
(sdedr:define-refeval-window "ref.dplayer1" "Rectangle"  (position 0 0 0) (position (- dnlL 3) dndepth 0)) 
(sdedr:define-refeval-window "ref.dplayer2" "Rectangle"  (position (+ dnlR 3) 0 0) (position (- dnmidL 3) dndepth 0)) 
(sdedr:define-refeval-window "ref.dplayer3" "Rectangle"  (position (+ dnmidR 3) 0 0) (position (- dnrL 3) dndepth 0)) 
(sdedr:define-refeval-window "ref.dplayer4" "Rectangle"  (position (+ dnrR 3)0 0) (position @bulkW@ dndepth 0)) 

; dntub
(sdedr:define-refeval-window "ref.dntub" "Rectangle"  (position (+ dnmidL 3) 0 0) (position (- dnmidR 3) dndepth 0))
(sdedr:define-refeval-window "ref.dntub.l" "Rectangle"  (position (+ dnlL 3) 0 0) (position (- dnlR 3) dndepth 0))
(sdedr:define-refeval-window "ref.dntub.r" "Rectangle"  (position (+ dnrL 3) 0 0) (position (- dnrR 3) dndepth 0))

; dptub
(sdedr:define-refeval-window "ref.dptub.l1" "Rectangle"  (position dplL 0 0) (position dplR dpdepth 0))
(sdedr:define-refeval-window "ref.dptub.l2" "Rectangle"  (position (+ dplL structW) 0 0) (position (+ dplR structW) dpdepth 0))
(sdedr:define-refeval-window "ref.dptub.r1" "Rectangle"  (position dprL 0 0) (position dprR dpdepth 0))
(sdedr:define-refeval-window "ref.dptub.r2" "Rectangle"  (position (+ dprL structW) 0 0) (position (+ dprR structW) dpdepth 0))
(sdedr:define-refeval-window "ref.dptub.mid" "Rectangle"  (position dpmidL 0 0) (position dpmidR dpdepth 0))

; sptub
(sdedr:define-refeval-window "ref.sptub.l1" "Rectangle"  (position dplL 0 0) (position dplR spdepth 0))
(sdedr:define-refeval-window "ref.sptub.l2" "Rectangle"  (position (+ dplL structW) 0 0) (position (+ dplR structW) spdepth 0))
(sdedr:define-refeval-window "ref.sptub.r1" "Rectangle"  (position dprL 0 0) (position dprR spdepth 0))
(sdedr:define-refeval-window "ref.sptub.r2" "Rectangle"  (position (+ dprL structW) 0 0) (position (+ dprR structW) spdepth 0))
(sdedr:define-refeval-window "ref.sptub.mid" "Rectangle"  (position spmidL 0 0) (position spmidR spdepth 0))


; sntub
(sdedr:define-refeval-window "ref.sntub.l" "Rectangle"  (position (+ dnlL 2) 0 0) (position (- dnlR 2) sndepth 0))
(sdedr:define-refeval-window "ref.sntub.r" "Rectangle"  (position (+ dnrL 2) 0 0) (position (- dnrR 2) sndepth 0))
(sdedr:define-refeval-window "ref.sntub.mid1" "Rectangle"  (position (+ dnmidL gapmidW) 0 0) (position (+ dnmidL gapmidW npmidW 0.1) sndepth 0))
(sdedr:define-refeval-window "ref.sntub.mid2" "Rectangle"  (position snmidL 0 0) (position snmidR sndepth 0))

; --- n+ ---
(sdedr:define-refeval-window "ref.nplus.l" "Rectangle"  (position (+ snlL 0.5) 0 0) (position (- snlR 0.5) nplusdepth 0))
(sdedr:define-refeval-window "ref.nplus.r" "Rectangle"  (position (+ snrL 0.5) 0 0) (position (- snrR 0.5) nplusdepth 0))
(sdedr:define-refeval-window "ref.nplus.mid1" "Rectangle"  (position (+ dnmidL gapmidW 0.05) 0 0) (position (+ dnmidL gapmidW 0.05 npmidW) nplusdepth 0))
(sdedr:define-refeval-window "ref.nplus.mid2" "Rectangle"  (position (+ spmidL ppmidW) 0 0) (position (- spmidR ppmidW) nplusdepth 0))
(sdedr:define-refeval-window "ref.nplus.mid3" "Rectangle"  (position (+ snmidL 0.05) 0 0) (position (+ snmidL 0.05 npmidW) nplusdepth 0))
(sdedr:define-refeval-window "ref.nplus.mid4" "Rectangle"  (position (- snmidR 0.05 npmidW) 0 0) (position (- snmidR 0.05) nplusdepth 0))


; --- p+ ---
(sdedr:define-refeval-window "ref.pplus.l1" "Rectangle"  (position pplL 0 0) (position pplR pplusdepth 0))
(sdedr:define-refeval-window "ref.pplus.l2" "Rectangle"  (position (+ pplL structW) 0 0) (position (+ pplR structW) pplusdepth 0))
(sdedr:define-refeval-window "ref.pplus.r1" "Rectangle"  (position pprL 0 0) (position pprR pplusdepth 0))
(sdedr:define-refeval-window "ref.pplus.r2" "Rectangle"  (position (+ pprL structW) 0 0) (position (+ pprR structW) pplusdepth 0))
(sdedr:define-refeval-window "ref.pplus.mid1" "Rectangle"  (position spmidL 0 0) (position (+ spmidL ppmidW) pplusdepth 0))
(sdedr:define-refeval-window "ref.pplus.mid2" "Rectangle"  (position (- spmidR ppmidW) 0 0) (position spmidR pplusdepth 0))
(sdedr:define-refeval-window "ref.pplus.mid3" "Rectangle"  (position (+ snmidL 1.25) 0 0) (position (- snmidR 1.25) pplusdepth 0))


; ===== DOPING PROFILES =====
(sdedr:define-constant-profile "CP.substrate" "BoronActiveConcentration" at_res_@resistivity@)
(sdedr:define-constant-profile-placement "placeCP.substrate" "CP.substrate" "ref.substrate")

; --- EXTRA DP LAYER --- (same profile as dntub)
(sdedr:define-gaussian-profile "AP.dplayer" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 1e17 "ValueAtDepth" at_res_@resistivity@ "Depth" 3 "Gauss"  "Factor" 0.8)
(sdedr:define-analytical-profile-placement "placeAP.dplayer1" "AP.dplayer" "ref.dplayer1" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.dplayer2" "AP.dplayer" "ref.dplayer2" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.dplayer3" "AP.dplayer" "ref.dplayer3" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.dplayer4" "AP.dplayer" "ref.dplayer4" "Both" "NoReplace" "Eval")

; --- DNTUB PROFILE ---
(sdedr:define-gaussian-profile "AP.dntub" "PhosphorusActiveConcentration" "PeakPos" 0  "PeakVal" 1e17 "ValueAtDepth" at_res_@resistivity@ "Depth" 3 "Gauss"  "Factor" 0.8)
(sdedr:define-analytical-profile-placement "placeAP.dntub" "AP.dntub" "ref.dntub" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.dntub.l" "AP.dntub" "ref.dntub.l" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.dntub.r" "AP.dntub" "ref.dntub.r" "Both" "NoReplace" "Eval")

; --- DPTUB PROFILE ---
(sdedr:define-gaussian-profile "AP.dptub" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 1e17 "ValueAtDepth" 1e14 "Depth" 1 "Gauss"  "Factor" 0.8)
(sdedr:define-analytical-profile-placement "placeAP.dptub.l1" "AP.dptub" "ref.dptub.l1" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.dptub.l2" "AP.dptub" "ref.dptub.l2" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.dptub.r1" "AP.dptub" "ref.dptub.r1" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.dptub.r2" "AP.dptub" "ref.dptub.r2" "Both" "NoReplace" "Eval")

(sdedr:define-gaussian-profile "AP.dptub.mid" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 1e17 "ValueAtDepth" 1e14 "Depth" 1 "Gauss"  "Factor" 0.8)
(sdedr:define-analytical-profile-placement "placeAP.dptub.mid" "AP.dptub.mid" "ref.dptub.mid" "Both" "NoReplace" "Eval")

; sptub
(sdedr:define-gaussian-profile "AP.sptub" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 1e18 "ValueAtDepth" 1e15 "Depth" 1 "Gauss"  "Factor" 0.8)
(sdedr:define-analytical-profile-placement "placeAP.sptub.l1" "AP.sptub" "ref.sptub.l1" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.sptub.l2" "AP.sptub" "ref.sptub.l2" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.sptub.r1" "AP.sptub" "ref.sptub.r1" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.sptub.r2" "AP.sptub" "ref.sptub.r2" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.sptub.mid" "AP.sptub" "ref.sptub.mid" "Both" "NoReplace" "Eval")

; sntub
(sdedr:define-gaussian-profile "AP.sntub" "PhosphorusActiveConcentration" "PeakPos" 0  "PeakVal" 1e18 "ValueAtDepth" 1e15 "Depth" 1 "Gauss"  "Factor" 0.8)
(sdedr:define-analytical-profile-placement "placeAP.sntub.mid1" "AP.sntub" "ref.sntub.mid1" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.sntub.mid2" "AP.sntub" "ref.sntub.mid2" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.sntub.l" "AP.sntub" "ref.sntub.l" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.sntub.r" "AP.sntub" "ref.sntub.r" "Both" "NoReplace" "Eval")

; --- n+ profile ---
(sdedr:define-gaussian-profile "AP.nplus" "PhosphorusActiveConcentration" "PeakPos" 0  "PeakVal" 1e19 "ValueAtDepth" 1e16 "Depth" 0.2 "Gauss"  "Factor" 0.8)
(sdedr:define-analytical-profile-placement "placeAP.nplus.l" "AP.nplus" "ref.nplus.l" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.nplus.r" "AP.nplus" "ref.nplus.r" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.nplus.mid1" "AP.nplus" "ref.nplus.mid1" "Both" "NoReplace" "Eval")
;(sdedr:define-analytical-profile-placement "placeAP.nplus.mid2" "AP.nplus" "ref.nplus.mid2" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.nplus.mid3" "AP.nplus" "ref.nplus.mid3" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.nplus.mid4" "AP.nplus" "ref.nplus.mid4" "Both" "NoReplace" "Eval")


; --- p+ profile ---
(sdedr:define-gaussian-profile "AP.pplus" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 1e19 "ValueAtDepth" 1e16 "Depth" 0.2 "Gauss"  "Factor" 0.8)
(sdedr:define-analytical-profile-placement "placeAP.pplus.l1" "AP.pplus" "ref.pplus.l1" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.pplus.l2" "AP.pplus" "ref.pplus.l2" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.pplus.r1" "AP.pplus" "ref.pplus.r1" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.pplus.r2" "AP.pplus" "ref.pplus.r2" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.pplus.mid1" "AP.pplus" "ref.pplus.mid1" "Both" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "placeAP.pplus.mid2" "AP.pplus" "ref.pplus.mid2" "Both" "NoReplace" "Eval")
;(sdedr:define-analytical-profile-placement "placeAP.pplus.mid3" "AP.pplus" "ref.pplus.mid3" "Both" "NoReplace" "Eval")

; === OXIDE ===
(sdegeo:create-rectangle (position 0 0 0 )  (position @bulkW@ @oxideT@ 0 ) "Oxide" "r.oxide" )

; === ALUMINUM CONTACTS ===
; --- HV ---
(sdegeo:create-rectangle (position (+ pplL 0.5) 0 0.0 )  (position (+ pplR -0.5) aludepth 0.0 ) "Aluminum" "r.hvl1" )
(sdegeo:create-rectangle (position (+ pplL 0.5 structW) 0 0.0 )  (position (+ pplR -0.5 structW) aludepth 0.0 ) "Aluminum" "r.hvl2" )
(sdegeo:create-rectangle (position (+ pprL 0.5) 0 0.0 )  (position (+ pprR -0.5) aludepth 0.0 ) "Aluminum" "r.hvr1" )
(sdegeo:create-rectangle (position (+ pprL 0.5 structW) 0 0.0 )  (position (+ pprR -0.5 structW) aludepth 0.0 ) "Aluminum" "r.hvr2" )

; --- VDD ---
(sdegeo:create-rectangle (position (+ nplL 0.5) 0 0.0 )  (position (+ nplL 0.5 3) aludepth 0.0 ) "Aluminum" "r.vddl1" )
(sdegeo:create-rectangle (position (- nplR 0.5 3) 0 0.0 )  (position (- nplR 0.5) aludepth 0.0 ) "Aluminum" "r.vddl2" )
(sdegeo:create-rectangle (position (+ nprL 0.5) 0 0.0 )  (position (+ nprL 0.5 3) aludepth 0.0 ) "Aluminum" "r.vddr1" )
(sdegeo:create-rectangle (position (- nprR 0.5 3) 0 0.0 )  (position (- nprR 0.5) aludepth 0.0 ) "Aluminum" "r.vddr2" )

(sdegeo:create-rectangle (position (+ dnmidL gapmidW 0.05 0.2) 0 0.0 )  (position (+ dnmidL gapmidW 0.05 npmidW -0.2) aludepth 0.0 ) "Aluminum" "r.vddmid1" )
(sdegeo:create-rectangle (position (+ snmidL 0.2) 0 0.0 )  (position (+ snmidL npmidW -0.2) aludepth 0.0 ) "Aluminum" "r.vddmid2" )
(sdegeo:create-rectangle (position (+ (- snmidR npmidW) 0.2) 0 0.0 )  (position (+ snmidR -0.2) aludepth 0.0 ) "Aluminum" "r.vddmid3" )

; --- VSS ---
(sdegeo:create-rectangle (position (+ spmidL 0.2) 0 0.0 )  (position (+ spmidL ppmidW -0.2) aludepth 0.0 ) "Aluminum" "r.vss1" )
(sdegeo:create-rectangle (position (+ (- spmidR ppmidW) 0.2) 0 0.0 )  (position (+ spmidR -0.2) aludepth 0.0 ) "Aluminum" "r.vss2" )


; create contacts
(sdegeo:define-contact-set "HV" 4  (color:rgb 1 0 0 ) "##" )
(sdegeo:define-contact-set "VSS" 4  (color:rgb 0 1 0 ) "##" )
(sdegeo:define-contact-set "VDD" 4  (color:rgb 0 0 1 ) "##" )

; assign contacts
(sdegeo:set-current-contact-set "HV")
(sdegeo:set-contact-edges (list (car (find-edge-id (position (+ pplL (/ ppW 2)) 0 0))) (car (find-edge-id (position (+ pplL (/ ppW 2) structW) 0 0))) (car (find-edge-id (position (+ pprL (/ ppW 2)) 0 0))) (car (find-edge-id (position (+ pprL (/ ppW 2) structW) 0 0)))) "HV")
(sdegeo:set-current-contact-set "VSS")
(sdegeo:set-contact-edges (list (car (find-edge-id (position (+ spmidL (/ ppmidW 2)) 0 0))) (car (find-edge-id (position (- spmidR (/ ppmidW 2)) 0 0)))) "VSS")
(sdegeo:set-current-contact-set "VDD")
(sdegeo:set-contact-edges (list (car (find-edge-id (position (+ nplL 2) 0 0))) (car (find-edge-id (position (- nplR 2) 0 0))) (car (find-edge-id (position (+ dnmidL gapmidW (/ npmidW 2)) 0 0))) (car (find-edge-id (position (+ snmidL (/ npmidW 2)) 0 0))) (car (find-edge-id (position (- snmidR (/ npmidW 2)) 0 0))) (car (find-edge-id (position (+ nprL 2) 0 0))) (car (find-edge-id (position (- nprR 2) 0 0)))) "VDD")


; mesh refinement
(sdedr:define-refinement-size "rf.substrate" 5 5 0.05 0.05 )
(sdedr:define-refinement-placement "placeRF.substrate" "rf.substrate" (list "region" "r.substrate" ) )
(sdedr:define-refinement-function "rf.substrate" "DopingConcentration" "MaxTransDiff" 1)

; meshing
(sde:set-meshing-command "snmesh -a -c boxmethod")
(sdedr:append-cmd-file "")
(sde:build-mesh "snmesh" "-a -c boxmethod" "@pwd@/n@node@")
(sde:save-model "@pwd@/n@node@")

exit
