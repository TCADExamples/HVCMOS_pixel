## THIS IS A MACHINE GENERATED COMMAND FILE FOR INSPECT
#set name rad@fluence@neq_@HV@V_@resistivity@ohm

proj_load @pwd@/@name@.plt @name@
cv_createDS NO_NAME {@name@ HV OuterVoltage} {@name@ VDD TotalCurrent} y
cv_write txt output/IV_@name@.txt TotalCurrent_VDD
proj_unload @name@

### vdd???
#if @particle@ == "on"
	proj_load @pwd@/particle_@name@.plt particle_@name@
	cv_createDS NO_NAME {particle_@name@ NO_NODE time} {particle_@name@ VDD TotalCurrent} y
	cv_write txt output/CC_@name@.txt TotalCurrent_VDD
#endif


exit
